defmodule Avin.PageController do
  use Avin.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
