module Weather exposing (view, Model)

import Html exposing (Html, div, strong, text)
import Html.Attributes exposing (class)


type alias Model =
    { day : String, date : String, high : String, low : String }


view : Model -> Html a
view model =
    div [ class "weather" ]
        [ div [] [ strong [] [ text model.day ] ]
        , div [] [ text model.date ]
        , div [] [ text model.high ]
        , div [] [ text model.low ]
        ]
