module Components.WeatherList exposing (..)

import Html exposing (Html, text, ul, li, div, h2, button)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)
import List
import Weather


type alias Model =
    { weatherForecast : List Weather.Model }


type Msg
    = NoOp
    | Fetch


update : Msg -> Model -> ( Model, Cmd Msg )
update message model =
    case message of
        NoOp ->
            ( model, Cmd.none )

        Fetch ->
            ( weatherForecast, Cmd.none )


weatherForecast : Model
weatherForecast =
    { weatherForecast =
        [ { day = "Monday", date = "6 September 2016", high = "15", low = "8" }
        , { day = "Tuesday", date = "7 September 2016", high = "13", low = "7" }
        , { day = "Wednesday", date = "8 September 2016", high = "18", low = "11" }
        ]
    }


initialModel : Model
initialModel =
    { weatherForecast = [] }


renderWeather : Weather.Model -> Html a
renderWeather weather =
    li [] [ Weather.view weather ]


renderWeatherForecast : Model -> List (Html a)
renderWeatherForecast model =
    List.map renderWeather model.weatherForecast


view : Model -> Html Msg
view model =
    div [ class "weather-list" ]
        [ h2 [] [ text "Weather" ]
        , button [ onClick Fetch, class "btn btn-primary" ] [ text "Fetch Forecast" ]
        , ul [] (renderWeatherForecast model)
        ]
