module Main exposing (..)

import Html exposing (Html, text, div)
import Html.Attributes exposing (class)
import Components.WeatherList as WeatherList
import Html.App


type alias Model =
    { weatherListModel : WeatherList.Model }


initialModel : Model
initialModel =
    { weatherListModel = WeatherList.initialModel }


type Msg
    = WeatherListMsg WeatherList.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        WeatherListMsg weatherMsg ->
            let
                ( updatedModel, cmd ) =
                    WeatherList.update weatherMsg model.weatherListModel
            in
                ( { model | weatherListModel = updatedModel }, Cmd.map WeatherListMsg cmd )


init : ( Model, Cmd Msg )
init =
    ( initialModel, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Html Msg
view model =
    div [ class "elm-app" ]
        [ Html.App.map WeatherListMsg (WeatherList.view model.weatherListModel) ]


main : Program Never
main =
    Html.App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
